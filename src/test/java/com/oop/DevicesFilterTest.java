package test.java.com.oop;

import main.java.com.oop.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class DevicesFilterTest {
    private final DevicesFilter cut = new DevicesFilter();

    static Memory memory1 = new Memory();
    static Memory memory2 = new Memory();
    static Memory memory3 = new Memory();
    static Memory memory4 = new Memory();
    static Memory memory5 = new Memory();
    static Memory memory6 = new Memory();
    static Memory memory7 = new Memory();
    static Memory memory8 = new Memory();
    static Memory memory9 = new Memory();
    static Memory memory10 = new Memory();

    static String[] memoryArray1 = {null, null, null, null, null, null, null, null, null, null, null};
    static String[] memoryArray2 = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", "iii", "kkk"};
    static String[] memoryArray3 = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", null, null};
    static String[] memoryArray4 = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", null, null, null};
    static String[] memoryArray5 = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", null, null, null, null};
    static String[] memoryArray6 = {"aaa", "bbb", "ccc", "ddd", "eee", null, null, null, null, null};
    static String[] memoryArray7 = {"aaa", "bbb", "ccc", "ddd", null, null, null, null, null, null};
    static String[] memoryArray8 = {"aaa", "bbb", "ccc", null, null, null, null, null, null, null};
    static String[] memoryArray9 = {"aaa", "bbb", null, null, null, null, null, null, null};
    static String[] memoryArray10 = {"aaa", null, null, null, null, null, null, null, null, null};

    static Processor processor1 = new ProcessorArm();
    static Processor processor2 = new ProcessorX86();
    static Processor processor3 = new ProcessorX86();
    static Processor processor4 = new ProcessorArm();
    static Processor processor5 = new ProcessorArm();
    static Processor processor6 = new ProcessorX86();
    static Processor processor7 = new ProcessorArm();
    static Processor processor8 = new ProcessorArm();
    static Processor processor9 = new ProcessorX86();
    static Processor processor10 = new ProcessorX86();

    static Device device1 = new Device();
    static Device device2 = new Device();
    static Device device3 = new Device();
    static Device device4 = new Device();
    static Device device5 = new Device();
    static Device device6 = new Device();
    static Device device7 = new Device();
    static Device device8 = new Device();
    static Device device9 = new Device();
    static Device device10 = new Device();

    static Device[] devices;

    static {
        processor1.cache = 130;
        processor1.bitCapacity = 16;
        processor1.frequency = 1;
        processor2.cache = 90;
        processor2.bitCapacity = 16;
        processor2.frequency = 2;
        processor3.cache = 200;
        processor3.bitCapacity = 32;
        processor3.frequency = 1;
        processor4.cache = 50;
        processor4.bitCapacity = 16;
        processor4.frequency = 2;
        processor5.cache = 75;
        processor5.bitCapacity = 32;
        processor5.frequency = 3;
        processor6.cache = 135;
        processor6.bitCapacity = 16;
        processor6.frequency = 3;
        processor7.cache = 320;
        processor7.bitCapacity = 16;
        processor7.frequency = 2;
        processor8.cache = 150;
        processor8.bitCapacity = 32;
        processor8.frequency = 3;
        processor9.cache = 100;
        processor9.bitCapacity = 32;
        processor9.frequency = 2;
        processor10.cache = 90;
        processor10.bitCapacity = 16;
        processor10.frequency = 2;

        memory1.memoryCell = memoryArray1;
        memory2.memoryCell = memoryArray2;
        memory3.memoryCell = memoryArray3;
        memory4.memoryCell = memoryArray4;
        memory5.memoryCell = memoryArray5;
        memory6.memoryCell = memoryArray6;
        memory7.memoryCell = memoryArray7;
        memory8.memoryCell = memoryArray8;
        memory9.memoryCell = memoryArray9;
        memory10.memoryCell = memoryArray10;

        device1.memory = memory1;
        device1.processor = processor1;
        device2.memory = memory2;
        device2.processor = processor2;
        device3.memory = memory3;
        device3.processor = processor3;
        device4.memory = memory4;
        device4.processor = processor4;
        device5.memory = memory5;
        device5.processor = processor5;
        device6.memory = memory6;
        device6.processor = processor6;
        device7.memory = memory7;
        device7.processor = processor7;
        device8.memory = memory8;
        device8.processor = processor8;
        device9.memory = memory9;
        device9.processor = processor9;
        device10.memory = memory10;
        device10.processor = processor10;

        devices = new Device[]{device1, device2, device3, device4, device5, device6, device7, device8, device9, device10};
    }

    static Arguments[] getDevicesX86TestArgs(){
        return new Arguments[]{
          Arguments.arguments(devices, new Device[]{device2, device3, device6, device9, device10})
        };
    }

    @ParameterizedTest
    @MethodSource("getDevicesX86TestArgs")
    void getDevicesX86Test(Device[] devicesArg, Device[] expected){
        Device[] actual = cut.getDevicesX86(devicesArg);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] getDevicesWithLargeCacheTestArgs(){
        return new Arguments[]{
                Arguments.arguments(devices, new Device[]{device1, device3, device6, device7, device8, device9})
        };
    }

    @ParameterizedTest
    @MethodSource("getDevicesWithLargeCacheTestArgs")
    void getDevicesWithLargeCacheTest(Device[] devicesArg, Device[] expected){
        Device[] actual = cut.getDevicesWithLargeCache(devicesArg);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] getDevicesWithEnoughMemoryTestArgs(){
        return new Arguments[]{
                Arguments.arguments(devices, 10, new Device[]{device1, device2, device3, device4, device5, device6,
                        device7, device8, device10})
        };
    }

    @ParameterizedTest
    @MethodSource("getDevicesWithEnoughMemoryTestArgs")
    void getDevicesWithEnoughMemoryTest(Device[] devicesArg, int capacity, Device[] expected){
        Device[] actual = cut.getDevicesWithEnoughMemory(devicesArg, capacity);
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] getDevicesWithOveroccupiedMemoryTestArgs(){
        return new Arguments[]{
                Arguments.arguments(devices, 45, new Device[]{device2, device3, device4, device5, device6})
        };
    }

    @ParameterizedTest
    @MethodSource("getDevicesWithOveroccupiedMemoryTestArgs")
    void getDevicesWithOveroccupiedMemoryTest(Device[] devicesArg, int occupiedMemory, Device[] expected){
        Device[] actual = cut.getDevicesWithOveroccupiedMemory(devicesArg, occupiedMemory);
        Assertions.assertArrayEquals(expected, actual);
    }
}
