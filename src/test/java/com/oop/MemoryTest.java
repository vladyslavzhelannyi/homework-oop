package test.java.com.oop;

import main.java.com.oop.Memory;
import main.java.com.oop.MemoryInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MemoryTest {
    private final Memory cut = new Memory(5);

    static Arguments[] readLastTestArgs(){
        return new Arguments[]{
          Arguments.arguments(new String[]{"aaa", "bbb", null, null, null}, "bbb"),
          Arguments.arguments(new String[]{"aaa", "bbb", "ccc", "ddd"}, "ddd"),
          Arguments.arguments(new String[]{null, null, null}, "Memory is empty")
        };
    }

    static Arguments[] removeLastTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"aaa", "bbb", null, null, null}, "bbb",
                        new String[]{"aaa", null, null, null, null}),
                Arguments.arguments(new String[]{"aaa", "bbb", "ccc", "ddd"}, "ddd",
                        new String[]{"aaa", "bbb", "ccc", null}),
                Arguments.arguments(new String[]{null, null, null, null}, "Memory is empty",
                        new String[]{null, null, null, null})
        };
    }

    static Arguments[] saveTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"aaa", "bbb", null, null, null}, "xxx",
                        new String[]{"aaa", "bbb", "xxx", null, null}, true),
                Arguments.arguments(new String[]{"aaa", "bbb", "ccc", "ddd"}, "fff",
                        new String[]{"aaa", "bbb", "ccc", "ddd"}, false),
                Arguments.arguments(new String[]{null, null, null, null}, "yyy",
                        new String[]{"yyy", null, null, null}, true)
        };
    }

    static Arguments[] getMemoryInfoTestArgs(){

        return new Arguments[]{
                Arguments.arguments(new String[]{"aaa", "bbb", null, null, null}, new MemoryInfo(3, 40)),
                Arguments.arguments(new String[]{"aaa", "bbb", "ccc", "ddd", "eee"}, new MemoryInfo(0, 100)),
                Arguments.arguments(new String[]{null, null, null, null, null}, new MemoryInfo(5, 0))
        };
    }

    @ParameterizedTest
    @MethodSource("readLastTestArgs")
    void readLastTest(String[] memoryArray, String expected){
        cut.memoryCell = memoryArray;
        String actual = cut.readLast();
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("removeLastTestArgs")
    void removeLastTest(String[] memoryArray, String expected, String[] expectedArray){
        cut.memoryCell = memoryArray;
        String actual = cut.removeLast();
        Assertions.assertEquals(expected, actual);
        String[] actualArray = cut.memoryCell;
        Assertions.assertArrayEquals(expectedArray, actualArray);
    }

    @ParameterizedTest
    @MethodSource("saveTestArgs")
    void saveTest(String[] memoryArray, String stringToSave, String[] expectedArray, boolean expected){
        cut.memoryCell = memoryArray;
        boolean actual = cut.save(stringToSave);
        String[] actualArray = cut.memoryCell;
        Assertions.assertEquals(expected, actual);
        Assertions.assertArrayEquals(expectedArray, actualArray);
    }

    @ParameterizedTest
    @MethodSource("getMemoryInfoTestArgs")
    void getMemoryInfoTest(String[] memoryArray, MemoryInfo expected){
        cut.memoryCell = memoryArray;
        MemoryInfo actual = cut.getMemoryInfo();
        Assertions.assertEquals(expected, actual);
    }
}
