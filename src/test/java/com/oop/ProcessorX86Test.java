package test.java.com.oop;

import main.java.com.oop.ProcessorArm;
import main.java.com.oop.ProcessorX86;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ProcessorX86Test {
    private final ProcessorX86 cut = new ProcessorX86();

    static Arguments[] dataProcessStringTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Validation", "validation"),
                Arguments.arguments("prOcEssOr", "processor")
        };
    }

    static Arguments[] dataProcessLongTestArgs(){
        return new Arguments[]{
                Arguments.arguments(5, "2"),
                Arguments.arguments(0, "0"),
                Arguments.arguments(-14, "-7"),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessStringTestArgs")
    void dataProcessStringTest(String data, String expected) {
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("dataProcessLongTestArgs")
    void dataProcessLongTest(long data, String expected) {
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected, actual);
    }
}
