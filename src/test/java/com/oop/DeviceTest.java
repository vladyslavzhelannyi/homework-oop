package test.java.com.oop;

import main.java.com.oop.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class DeviceTest {
    private final Device cut = new Device();

    static Memory memory1 = new Memory();
    static Memory memory2 = new Memory();

    static String[] memoryArray1 = {"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", null, null};
    static String[] memoryArray2 = {"AAA", "bbb", "ccc", "ddd", null, null, null, null, null, null};

    static Processor processor1 = new ProcessorArm();
    static Processor processor2 = new ProcessorX86();

    static {
        processor1.cache = 130;
        processor1.bitCapacity = 16;
        processor1.frequency = 1;
        processor2.cache = 90;
        processor2.bitCapacity = 16;
        processor2.frequency = 2;

        memory1.memoryCell = memoryArray1;
        memory2.memoryCell = memoryArray2;
    }

    @BeforeEach
    void deviceUp(){
        memoryArray1 = new String[]{"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", null, null};
        memoryArray2 = new String[]{"AAA", "bbb", "ccc", "ddd", null, null, null, null, null, null};

        processor1.cache = 130;
        processor1.bitCapacity = 16;
        processor1.frequency = 1;
        processor2.cache = 90;
        processor2.bitCapacity = 16;
        processor2.frequency = 2;

        memory1.memoryCell = memoryArray1;
        memory2.memoryCell = memoryArray2;
    }

    static Arguments[] saveTestDeviceArgs(){
        return new Arguments[]{
          Arguments.arguments(memory1, processor1, new String[]{"aaa", "bbb", "ccc"},
                  new String[]{"aaa", "bbb", "ccc", "ddd", "eee", "fff", "ggg", "hhh", "aaa", "bbb"}),
          Arguments.arguments(memory2, processor2, new String[]{"aaa", "bbb", "ccc", "ddd"},
                  new String[]{"AAA", "bbb", "ccc", "ddd", "aaa", "bbb", "ccc", "ddd", null, null})
        };
    }

    @ParameterizedTest
    @MethodSource("saveTestDeviceArgs")
    void saveTestDevice(Memory memoryArg, Processor processorArg, String[] data, String[] expected){
        cut.processor = processorArg;
        cut.memory = memoryArg;
        cut.save(data);
        String[] actual = cut.memory.memoryCell;
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] readAllTestArgs(){
        return new Arguments[]{
                Arguments.arguments(memory1, processor1,
                        new String[]{"hhh", "ggg", "fff", "eee", "ddd", "ccc", "bbb", "aaa"}),
                Arguments.arguments(memory2, processor2,
                        new String[]{"ddd", "ccc", "bbb", "AAA"})
        };
    }

    @ParameterizedTest
    @MethodSource("readAllTestArgs")
    void readAllTest(Memory memoryArg, Processor processorArg, String[] expected){
        cut.processor = processorArg;
        cut.memory = memoryArg;
        String[] actual = cut.readAll();
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] dataProcessingTestArgs(){
        return new Arguments[]{
                Arguments.arguments(memory1, processor1,
                        new String[]{"HHH", "GGG", "FFF", "EEE", "DDD", "CCC", "BBB", "AAA", null, null}),
                Arguments.arguments(memory2, processor2,
                        new String[]{"ddd", "ccc", "bbb", "aaa", null, null, null, null, null, null})
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessingTestArgs")
    void dataProcessingTest(Memory memoryArg, Processor processorArg, String[] expected){
        cut.processor = processorArg;
        cut.memory = memoryArg;
        cut.dataProcessing();
        String[] actual = cut.memory.memoryCell;
        Assertions.assertArrayEquals(expected, actual);
    }

    static Arguments[] getSystemInfoTestArgs(){
        return new Arguments[]{
                Arguments.arguments(memory1, processor1,
                        "Processor: frequency = 1, cache = 130, bitCapacity = 16. " +
                                "MemoryInfo: availableMemory = 2, occupiedMemory = 80. "),
                Arguments.arguments(memory2, processor2,
                        "Processor: frequency = 2, cache = 90, bitCapacity = 16. " +
                                "MemoryInfo: availableMemory = 6, occupiedMemory = 40. ")
        };
    }

    @ParameterizedTest
    @MethodSource("getSystemInfoTestArgs")
    void getSystemInfoTest(Memory memoryArg, Processor processorArg, String expected){
        cut.processor = processorArg;
        cut.memory = memoryArg;
        String actual = cut.getSystemInfo();
        Assertions.assertEquals(expected, actual);
    }
}
