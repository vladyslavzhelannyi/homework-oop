package test.java.com.oop;

import main.java.com.oop.ProcessorArm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Locale;

public class ProcessorArmTest {
    private final ProcessorArm cut = new ProcessorArm();

    static Arguments[] dataProcessStringTestArgs(){
        return new Arguments[]{
          Arguments.arguments("Validation", "VALIDATION"),
          Arguments.arguments("prOcEssOr", "PROCESSOR")
        };
    }

    static Arguments[] dataProcessLongTestArgs(){
        return new Arguments[]{
                Arguments.arguments(3, "6"),
                Arguments.arguments(0, "0"),
                Arguments.arguments(-16, "-32"),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessStringTestArgs")
    void dataProcessStringTest(String data, String expected) {
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected, actual);
    }

    @ParameterizedTest
    @MethodSource("dataProcessLongTestArgs")
    void dataProcessLongTest(long data, String expected) {
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected, actual);
    }
}
