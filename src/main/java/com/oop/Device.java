package main.java.com.oop;

import java.util.Objects;

public class Device {
    /*
    Создать класс Device, который содержит 2 поля:
●	Processor processor
●	Memmory memory
Создать класс Device, который содержит следующие методы:
●	void save (String[] data) – сохранение в память всех элементов в массиве
●	String[] readAll() – вычитка всех элементов из памяти, затем стирание данных
●	void dataProcessing() – преобразование всех данных, записанных в памяти
●	String getSystemInfo() – получение строки с информацией о системе (информация о процессоре, памяти)
     */

    public Processor processor;
    public Memory memory;

    public Device(){

    }

    public Device(Processor processor, Memory memory){
        this.processor = processor;
        this.memory = memory;
    }

    public void save(String[] data){
        for (int i = 0; i < data.length; i++){
            boolean isAdded = memory.save(data[i]);
            if (!isAdded){
                System.out.println("Memory ended");
                System.out.println(i + " strings were saved");
                break;
            }
        }
    }

    public String[] readAll(){
        String[] strings = new String[0];
        int occupiedMemory = memory.getMemoryInfo().getOccupiedMemory();
        while (occupiedMemory != 0){
            String string = memory.removeLast();
            strings = addStringToArray(strings, string);
            occupiedMemory = memory.getMemoryInfo().getOccupiedMemory();
        }
        return strings;
    }

    public void dataProcessing(){
        String[] strings = readAll();
        for (int i = 0; i < strings.length; i++){
            strings[i] = processor.dataProcess(strings[i]);
        }
        for (int i = 0; i < strings.length; i++){
            memory.save(strings[i]);
        }
    }

    public String getSystemInfo(){
        String systemInfo = processor.getDetails() + memory.getMemoryInfo().toString();
        return systemInfo;
    }

    private String[] addStringToArray(String[] strings, String string){
        String[] newStrings = new String[strings.length + 1];
        for (int i = 0; i < strings.length; i++){
            newStrings[i] = strings[i];
        }
        newStrings[strings.length] = string;
        return  newStrings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Device device = (Device) o;
        return Objects.equals(processor, device.processor) && Objects.equals(memory, device.memory);
    }

    @Override
    public int hashCode() {
        return Objects.hash(processor, memory);
    }
}
