package main.java.com.oop;

import java.util.Objects;

public class MemoryInfo {
    private int availableMemory;
    private int occupiedMemory;

    public MemoryInfo(){

    }

    public MemoryInfo(int availableMemory, int occupiedMemory){
        this.availableMemory = availableMemory;
        this.occupiedMemory = occupiedMemory;
    }

    public int getAvailableMemory() {
        return availableMemory;
    }

    public void setAvailableMemory(int availableMemory) {
        this.availableMemory = availableMemory;
    }

    public int getOccupiedMemory() {
        return occupiedMemory;
    }

    public void setOccupiedMemory(int occupiedMemory) {
        this.occupiedMemory = occupiedMemory;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MemoryInfo that = (MemoryInfo) o;
        return availableMemory == that.availableMemory && occupiedMemory == that.occupiedMemory;
    }

    @Override
    public int hashCode() {
        return Objects.hash(availableMemory, occupiedMemory);
    }

    @Override
    public String toString() {
        return "MemoryInfo: " +
                "availableMemory = " + availableMemory +
                ", occupiedMemory = " + occupiedMemory + ". ";
    }
}
