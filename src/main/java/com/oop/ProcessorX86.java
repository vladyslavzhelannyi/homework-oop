package main.java.com.oop;

import java.util.Locale;
import java.util.Objects;

public class ProcessorX86 extends Processor{
    public final String architecture = "X86";

    @Override
    public String dataProcess(String data) {
        String result = data.toLowerCase(Locale.ROOT);
        return result;
    }

    @Override
    public String dataProcess(long data) {
        data /= 2;
        String result = String.valueOf(data);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ProcessorX86 that = (ProcessorX86) o;
        return Objects.equals(architecture, that.architecture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), architecture);
    }
}
