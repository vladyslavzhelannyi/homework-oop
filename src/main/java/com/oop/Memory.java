package main.java.com.oop;

import java.util.Arrays;

public class Memory {
    /*Создать класс Memory, который содержит поле String[] memoryCell.
    Поле инициализируется в конструкторе. Начальные значения – null. Класс содержит 4 метода
●	String readLast() – вычитать значение из последнего записанного элемента массива (значение которого не null),
●	String removeLast() – удаление последнего элемента (записать значение null),
●	boolean save() – записать в последнюю ячейку, значение которой null, вернуть true,
если свободных ячеек памяти нет – вернуть false;
●	getMemoryInfo() – возвращает объект, состоящий из двух полей: общий объём памяти
(количество доступных ячеек), занятый объём памяти (в процентах).
*/
    public String[] memoryCell; //паблик для тестов без использования рефлексии

    public Memory(){

    }

    public Memory(int capacity){
        this.memoryCell = new String[capacity];
        Arrays.fill(memoryCell, null);
    }

    public String readLast(){
        String result = null;
        if (memoryCell.length == 0){
            result = "Memory capacity is 0";
        }
        else{
            for (int i = memoryCell.length - 1; i >= 0; i--){
                if (memoryCell[i] != null){
                    result = memoryCell[i];
                    break;
                }
            }
            if (result == null){
                result = "Memory is empty";
            }
        }
        return result;
    }

    public String removeLast(){
        String deletedString = null;
        if (memoryCell.length == 0){
            deletedString = "Memory capacity is 0";
        }
        else{
            for (int i = memoryCell.length - 1; i >= 0; i--){
                if (memoryCell[i] != null){
                    deletedString = memoryCell[i];
                    memoryCell[i] = null;
                    break;
                }
            }
            if (deletedString == null){
                deletedString = "Memory is empty";
            }
        }
        return deletedString;
    }

    public boolean save(String string){
        boolean isSaved = false;
        for (int i = 0; i < memoryCell.length; i++){
            if (memoryCell[i] == null){
                memoryCell[i] = string;
                isSaved = true;
                break;
            }
        }
        return isSaved;
    }

    public MemoryInfo getMemoryInfo(){
        MemoryInfo memoryInfo = new MemoryInfo();

        int availableMemory = 0;
        for (int i = memoryCell.length - 1; i >= 0; i--){
            if (memoryCell[i] == null){
                availableMemory++;
            }
        }

        int occupiedMemory = (int)(((double)memoryCell.length - (double)availableMemory) /
                (double)memoryCell.length * 100);
        memoryInfo.setAvailableMemory(availableMemory);
        memoryInfo.setOccupiedMemory(occupiedMemory);
        return memoryInfo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Memory memory = (Memory) o;
        return Arrays.equals(memoryCell, memory.memoryCell);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(memoryCell);
    }
}
