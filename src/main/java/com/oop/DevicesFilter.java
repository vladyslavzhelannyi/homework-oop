package main.java.com.oop;

public class DevicesFilter {
    public Device[] getDevicesX86(Device[] devices){
        Device[] newDevices = new Device[0];
        String processorArchitecture = null;
        for (int i = 0; i < devices.length; i++){
            Processor processor = devices[i].processor;
            if (processor instanceof ProcessorX86){
                newDevices = addDeviceToArray(newDevices, devices[i]);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithLargeCache(Device[] devices){
        Device[] newDevices = new Device[0];
        for (int i = 0; i < devices.length; i++){
            if (devices[i].processor.cache >= 100){
                newDevices = addDeviceToArray(newDevices, devices[i]);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithEnoughMemory(Device[] devices, int capacity){
        Device[] newDevices = new Device[0];
        for (int i = 0; i < devices.length; i++){
            int deviceCapacity = devices[i].memory.memoryCell.length;
            if(deviceCapacity >= capacity){
                newDevices = addDeviceToArray(newDevices, devices[i]);
            }
        }
        return newDevices;
    }

    public Device[] getDevicesWithOveroccupiedMemory(Device[] devices, int occupationPercent){
        Device[] newDevices = new Device[0];
        for (int i = 0; i < devices.length; i++){
            Memory deviceMemory = devices[i].memory;
            MemoryInfo deviceMemoryInfo = deviceMemory.getMemoryInfo();
            int deviceOccupiedMemory = deviceMemoryInfo.getOccupiedMemory();
            if (deviceOccupiedMemory >= occupationPercent){
                newDevices = addDeviceToArray(newDevices, devices[i]);
            }
        }
        return newDevices;
    }

    private Device[] addDeviceToArray(Device[] devices, Device device){
        Device[] newDevices = new Device[devices.length + 1];
        for (int i = 0; i < devices.length; i++){
            newDevices[i] = devices[i];
        }
        newDevices[devices.length] = device;
        return  newDevices;
    }
}
